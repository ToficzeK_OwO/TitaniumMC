plugins {
  `maven-publish`
  `kotlin-dsl`
}

group = "xyz.jpenilla"
version = "1.0.0-SNAPSHOT"

repositories {
  mavenCentral()
  jcenter()
  gradlePluginPortal()
}

dependencies {
  implementation("org.spongepowered", "configurate-hocon", "4.0.0")
  implementation("org.spongepowered", "configurate-extra-kotlin", "4.0.0")
  implementation("org.jetbrains.kotlinx", "kotlinx.dom", "0.0.10")
  implementation("com.github.jengelman.gradle.plugins", "shadow", "6.1.0")
}

java {
  sourceCompatibility = JavaVersion.toVersion(8)
  targetCompatibility = JavaVersion.toVersion(8)
  withSourcesJar()
}

kotlin {
  explicitApi()
}

tasks {
  compileKotlin {
    kotlinOptions.apiVersion = "1.4"
    kotlinOptions.jvmTarget = "1.8"
  }
}

gradlePlugin {
  plugins {
    create("Toothpick") {
      id = "xyz.jpenilla.toothpick"
      implementationClass = "xyz.jpenilla.toothpick.Toothpick"
    }
  }
}