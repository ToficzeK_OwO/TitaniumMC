From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Simon Gardling <titaniumtown@gmail.com>
Date: Tue, 20 Apr 2021 12:53:35 -0400
Subject: [PATCH] MCMT: Parallel World Ticking (unstable)

based off of: https://github.com/jediminer543/JMT-MCMT

Contains: Parallel World ticking

diff --git a/src/main/java/com/tuinity/tuinity/chunk/PlayerChunkLoader.java b/src/main/java/com/tuinity/tuinity/chunk/PlayerChunkLoader.java
index 0d577aa1c7868ce89c3902535adcb554b1f47551..32393183d60692c88ddab0d4813b273c9beec095 100644
--- a/src/main/java/com/tuinity/tuinity/chunk/PlayerChunkLoader.java
+++ b/src/main/java/com/tuinity/tuinity/chunk/PlayerChunkLoader.java
@@ -25,6 +25,7 @@ import java.util.ArrayList;
 import java.util.List;
 import java.util.TreeSet;
 import java.util.concurrent.atomic.AtomicInteger;
+import java.util.concurrent.ConcurrentSkipListSet;
 
 public final class PlayerChunkLoader {
 
@@ -38,7 +39,7 @@ public final class PlayerChunkLoader {
     protected final Reference2ObjectLinkedOpenHashMap<EntityPlayer, PlayerLoaderData> playerMap = new Reference2ObjectLinkedOpenHashMap<>(512, 0.7f);
     protected final ReferenceLinkedOpenHashSet<PlayerLoaderData> chunkSendQueue = new ReferenceLinkedOpenHashSet<>(512, 0.7f);
 
-    protected final TreeSet<PlayerLoaderData> chunkLoadQueue = new TreeSet<>((final PlayerLoaderData p1, final PlayerLoaderData p2) -> {
+    protected final ConcurrentSkipListSet<PlayerLoaderData> chunkLoadQueue = new ConcurrentSkipListSet<>((final PlayerLoaderData p1, final PlayerLoaderData p2) -> {
         if (p1 == p2) {
             return 0;
         }
@@ -62,7 +63,8 @@ public final class PlayerChunkLoader {
         return Integer.compare(System.identityHashCode(p1), System.identityHashCode(p2));
     });
 
-    protected final TreeSet<PlayerLoaderData> chunkSendWaitQueue = new TreeSet<>((final PlayerLoaderData p1, final PlayerLoaderData p2) -> {
+    // protected final TreeSet<PlayerLoaderData> chunkSendWaitQueue = new TreeSet<>((final PlayerLoaderData p1, final PlayerLoaderData p2) -> {
+    protected final ConcurrentSkipListSet<PlayerLoaderData> chunkSendWaitQueue = new ConcurrentSkipListSet<>((final PlayerLoaderData p1, final PlayerLoaderData p2) -> {
         if (p1 == p2) {
             return 0;
         }
@@ -429,7 +431,7 @@ public final class PlayerChunkLoader {
 
     protected static final AtomicInteger concurrentChunkSends = new AtomicInteger();
     protected final Reference2IntOpenHashMap<PlayerLoaderData> sendingChunkCounts = new Reference2IntOpenHashMap<>();
-    private void trySendChunks() {
+    private synchronized void trySendChunks() {
         final long time = System.nanoTime();
         // drain entries from wait queue
         while (!this.chunkSendWaitQueue.isEmpty()) {
@@ -510,7 +512,7 @@ public final class PlayerChunkLoader {
     }
 
     protected int concurrentChunkLoads;
-    private void tryLoadChunks() {
+    private synchronized void tryLoadChunks() {
         if (this.chunkLoadQueue.isEmpty()) {
             return;
         }
diff --git a/src/main/java/com/tuinity/tuinity/util/misc/Delayed8WayDistancePropagator2D.java b/src/main/java/com/tuinity/tuinity/util/misc/Delayed8WayDistancePropagator2D.java
index cdd3c4032c1d6b34a10ba415bd4d0e377aa9af3c..bb7b7aa7d49859cfeb9a1d21933aac443ea86e62 100644
--- a/src/main/java/com/tuinity/tuinity/util/misc/Delayed8WayDistancePropagator2D.java
+++ b/src/main/java/com/tuinity/tuinity/util/misc/Delayed8WayDistancePropagator2D.java
@@ -7,6 +7,7 @@ import it.unimi.dsi.fastutil.longs.LongArrayFIFOQueue;
 import it.unimi.dsi.fastutil.longs.LongIterator;
 import it.unimi.dsi.fastutil.longs.LongLinkedOpenHashSet;
 import net.minecraft.server.MCUtil;
+import org.jmt.mcmt.paralelised.fastutil.ConcurrentLongLinkedOpenHashSet; // Yatopia - MCMT
 
 public final class Delayed8WayDistancePropagator2D {
 
@@ -277,7 +278,7 @@ public final class Delayed8WayDistancePropagator2D {
 
     // Generally updates to positions are made close to other updates, so we link to decrease cache misses when
     // propagating updates
-    protected final LongLinkedOpenHashSet updatedSources = new LongLinkedOpenHashSet();
+    protected final LongLinkedOpenHashSet updatedSources = new ConcurrentLongLinkedOpenHashSet();
 
     @FunctionalInterface
     public static interface LevelChangeCallback {
diff --git a/src/main/java/net/himeki/mcmtfabric/ParallelProcessor.java b/src/main/java/net/himeki/mcmtfabric/ParallelProcessor.java
index 70e68e774d2c7815b565f8f3befda291a8394735..63effc9150aea2179b3a6d2b170a7d98665a6b3a 100644
--- a/src/main/java/net/himeki/mcmtfabric/ParallelProcessor.java
+++ b/src/main/java/net/himeki/mcmtfabric/ParallelProcessor.java
@@ -126,6 +126,24 @@ public class ParallelProcessor {
             YatopiaConfig.changeMCMTStatus(true);
             
             worldserver.doTick(hasTimeLeft);
+        } else {
+            phaser.register();
+            ex.execute(() -> {
+                try {
+                    currentWorldServers.incrementAndGet();
+                    worldserver.doTick(hasTimeLeft);
+        
+                    long currTime = System.nanoTime();
+                    if (!(currTime - worldserver.lastMidTickExecuteFailure <= minecraftServer.TASK_EXECUTION_FAILURE_BACKOFF)) {
+                        if (!worldserver.getChunkProvider().runTasks()) {
+                            worldserver.lastMidTickExecuteFailure = currTime;
+                        }
+                    }
+                } finally {
+                    phaser.arriveAndDeregister();
+                    currentWorldServers.decrementAndGet();
+                }
+            });
         }
 
     }
diff --git a/src/main/java/net/minecraft/server/MinecraftServer.java b/src/main/java/net/minecraft/server/MinecraftServer.java
index 6fb0673b463ed465c3e7eed2dc38de9631d41ffa..91da2133e248fb3c4103619f64f41f0f24d48247 100644
--- a/src/main/java/net/minecraft/server/MinecraftServer.java
+++ b/src/main/java/net/minecraft/server/MinecraftServer.java
@@ -1250,7 +1250,7 @@ public abstract class MinecraftServer extends IAsyncTaskHandlerReentrant<Runnabl
     static final long CHUNK_TASK_QUEUE_BACKOFF_MIN_TIME = 25L * 1000L; // 25us
     static final long MAX_CHUNK_EXEC_TIME = 1000L; // 1us
 
-    static final long TASK_EXECUTION_FAILURE_BACKOFF = 5L * 1000L; // 5us
+    public static final long TASK_EXECUTION_FAILURE_BACKOFF = 5L * 1000L; // 5us
 
     private static long lastMidTickExecute;
     private static long lastMidTickExecuteFailure;
@@ -1277,6 +1277,7 @@ public abstract class MinecraftServer extends IAsyncTaskHandlerReentrant<Runnabl
 
     public final void executeMidTickTasks() {
         org.spigotmc.AsyncCatcher.catchOp("mid tick chunk task execution");
+        if (!YatopiaConfig.disableWorld) return;
 
         long startTime = System.nanoTime();
         if ((startTime - lastMidTickExecute) <= CHUNK_TASK_QUEUE_BACKOFF_MIN_TIME || (startTime - lastMidTickExecuteFailure) <= TASK_EXECUTION_FAILURE_BACKOFF) {
diff --git a/src/main/java/net/minecraft/server/level/ChunkMapDistance.java b/src/main/java/net/minecraft/server/level/ChunkMapDistance.java
index fb4d006f86229fd093f1a9ea8cab2add0a4cacfa..543f9b5109692e82dc6fd911b92349590ff35b13 100644
--- a/src/main/java/net/minecraft/server/level/ChunkMapDistance.java
+++ b/src/main/java/net/minecraft/server/level/ChunkMapDistance.java
@@ -34,6 +34,7 @@ import net.minecraft.world.level.chunk.ChunkStatus;
 import org.apache.logging.log4j.LogManager;
 import org.apache.logging.log4j.Logger;
 import org.spigotmc.AsyncCatcher; // Paper
+import org.jmt.mcmt.paralelised.fastutil.sync.SyncLong2IntLinkedOpenHashMap;
 
 public abstract class ChunkMapDistance {
 
@@ -104,7 +105,7 @@ public abstract class ChunkMapDistance {
     }
     // Tuinity end - delay chunk unloads
     // Tuinity start - replace ticket level propagator
-    protected final Long2IntLinkedOpenHashMap ticketLevelUpdates = new Long2IntLinkedOpenHashMap() {
+    protected final SyncLong2IntLinkedOpenHashMap ticketLevelUpdates = new SyncLong2IntLinkedOpenHashMap() { // Yatopia - use 
         @Override
         protected void rehash(int newN) {
             // no downsizing allowed
@@ -212,7 +213,7 @@ public abstract class ChunkMapDistance {
     protected abstract PlayerChunk a(long i, int j, @Nullable PlayerChunk playerchunk, int k); protected final PlayerChunk updateTicketLevel(long coord, int newLevel, @Nullable PlayerChunk playerchunk, int oldLevel) { return this.a(coord, newLevel, playerchunk, oldLevel); } // Tuinity - OBFHELPER
 
     protected long ticketLevelUpdateCount; // Tuinity - replace ticket level propagator
-    public boolean a(PlayerChunkMap playerchunkmap) {
+    public synchronized boolean a(PlayerChunkMap playerchunkmap) {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("Cannot tick ChunkMapDistance off of the main-thread");// Tuinity
         //this.f.a(); // Paper - no longer used
         //AsyncCatcher.catchOp("DistanceManagerTick"); // Paper // Airplane - leave up to softEnsures
@@ -391,7 +392,7 @@ public abstract class ChunkMapDistance {
         return false;
     }
 
-    private boolean addPriorityTicket(ChunkCoordIntPair coords, TicketType<ChunkCoordIntPair> ticketType, int priority) {
+    private synchronized boolean addPriorityTicket(ChunkCoordIntPair coords, TicketType<ChunkCoordIntPair> ticketType, int priority) {
         AsyncCatcher.catchOp("ChunkMapDistance::addPriorityTicket");
         long pair = coords.pair();
         PlayerChunk chunk = chunkMap.getUpdatingChunk(pair);
diff --git a/src/main/java/net/minecraft/server/level/PlayerChunkMap.java b/src/main/java/net/minecraft/server/level/PlayerChunkMap.java
index d2b2db467f44f67ad5ffaf83422925a147411497..0e596debce736398268e78345204773055a0c116 100644
--- a/src/main/java/net/minecraft/server/level/PlayerChunkMap.java
+++ b/src/main/java/net/minecraft/server/level/PlayerChunkMap.java
@@ -111,6 +111,8 @@ import org.apache.logging.log4j.Logger;
 import org.bukkit.entity.Player; // CraftBukkit
 import org.spigotmc.AsyncCatcher;
 
+import org.jmt.mcmt.paralelised.fastutil.Long2ByteConcurrentHashMap;
+
 final public class PlayerChunkMap extends IChunkLoader implements PlayerChunk.d {
 
     private static final Logger LOGGER = LogManager.getLogger();
@@ -326,14 +328,14 @@ final public class PlayerChunkMap extends IChunkLoader implements PlayerChunk.d
             return this.navigators;
         }
 
-        public boolean addToNavigators(final EntityInsentient navigator) {
+        public synchronized boolean addToNavigators(final EntityInsentient navigator) {
             if (this.navigators == null) {
                 this.navigators = new com.tuinity.tuinity.util.maplist.IteratorSafeOrderedReferenceSet<>();
             }
             return this.navigators.add(navigator);
         }
 
-        public boolean removeFromNavigators(final EntityInsentient navigator) {
+        public synchronized boolean removeFromNavigators(final EntityInsentient navigator) {
             if (this.navigators == null) {
                 return false;
             }
@@ -352,7 +354,7 @@ final public class PlayerChunkMap extends IChunkLoader implements PlayerChunk.d
             return this.navigators;
         }
 
-        public boolean addToNavigators(final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.RegionSection section, final EntityInsentient navigator) {
+        public synchronized boolean addToNavigators(final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.RegionSection section, final EntityInsentient navigator) {
             if (this.navigators == null) {
                 this.navigators = new com.tuinity.tuinity.util.maplist.IteratorSafeOrderedReferenceSet<>();
             }
@@ -366,7 +368,7 @@ final public class PlayerChunkMap extends IChunkLoader implements PlayerChunk.d
             return ret;
         }
 
-        public boolean removeFromNavigators(final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.RegionSection section, final EntityInsentient navigator) {
+        public synchronized boolean removeFromNavigators(final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.RegionSection section, final EntityInsentient navigator) {
             if (this.navigators == null) {
                 return false;
             }
@@ -382,7 +384,7 @@ final public class PlayerChunkMap extends IChunkLoader implements PlayerChunk.d
         // Tuinity end - optimise notify()
 
         @Override
-        public void removeFromRegion(final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.RegionSection section,
+        public synchronized void removeFromRegion(final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.RegionSection section,
                                      final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.Region from) {
             final DataRegionSectionData sectionData = (DataRegionSectionData)section.sectionData;
             final DataRegionData fromData = (DataRegionData)from.regionData;
@@ -398,7 +400,7 @@ final public class PlayerChunkMap extends IChunkLoader implements PlayerChunk.d
         }
 
         @Override
-        public void addToRegion(final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.RegionSection section,
+        public synchronized void addToRegion(final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.RegionSection section,
                                 final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.Region oldRegion,
                                 final com.tuinity.tuinity.chunk.SingleThreadChunkRegionManager.Region newRegion) {
             final DataRegionSectionData sectionData = (DataRegionSectionData)section.sectionData;
@@ -427,7 +429,7 @@ final public class PlayerChunkMap extends IChunkLoader implements PlayerChunk.d
         this.u = new AtomicInteger();
         this.playerMap = new PlayerMap();
         this.trackedEntities = new Int2ObjectOpenHashMap();
-        this.z = new Long2ByteOpenHashMap();
+        this.z = new Long2ByteConcurrentHashMap();
         this.A = new com.destroystokyo.paper.utils.CachedSizeConcurrentLinkedQueue<>(); // Paper - need constant-time size()
         this.definedStructureManager = definedstructuremanager;
         this.w = convertable_conversionsession.a(worldserver.getDimensionKey());
@@ -856,7 +858,7 @@ final public class PlayerChunkMap extends IChunkLoader implements PlayerChunk.d
     }
 
     @Nullable
-    private PlayerChunk a(long i, int j, @Nullable PlayerChunk playerchunk, int k) {
+    private synchronized PlayerChunk a(long i, int j, @Nullable PlayerChunk playerchunk, int k) {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("Chunk holder update"); // Tuinity
         if (this.unloadingPlayerChunk) { MinecraftServer.LOGGER.fatal("Cannot tick distance manager while unloading playerchunks", new Throwable()); throw new IllegalStateException("Cannot tick distance manager while unloading playerchunks"); } // Tuinity
         if (k > PlayerChunkMap.GOLDEN_TICKET && j > PlayerChunkMap.GOLDEN_TICKET) {
@@ -1032,7 +1034,7 @@ final public class PlayerChunkMap extends IChunkLoader implements PlayerChunk.d
         gameprofilerfiller.exit();
     }
 
-    private void b(BooleanSupplier booleansupplier) {
+    private synchronized void b(BooleanSupplier booleansupplier) {
         LongIterator longiterator = this.unloadQueue.iterator();
         // Spigot start
         org.spigotmc.SlackActivityAccountant activityAccountant = this.world.getMinecraftServer().slackActivityAccountant;
@@ -2611,7 +2613,7 @@ Sections go from 0..16. Now whenever a section is not empty, it can potentially
         }
 
         @Override
-        protected boolean a(long i) {
+        protected synchronized boolean a(long i) {
             return PlayerChunkMap.this.unloadQueue.contains(i);
         }
 
diff --git a/src/main/java/net/minecraft/world/level/pathfinder/PathfinderAbstract.java b/src/main/java/net/minecraft/world/level/pathfinder/PathfinderAbstract.java
index f0400147cbfa9dedd7637bd5c8935b4dbf5e3bdd..8a2582dab38e0f908cc7cdc849a463e4b899c150 100644
--- a/src/main/java/net/minecraft/world/level/pathfinder/PathfinderAbstract.java
+++ b/src/main/java/net/minecraft/world/level/pathfinder/PathfinderAbstract.java
@@ -32,7 +32,7 @@ public abstract class PathfinderAbstract {
         this.f = MathHelper.d(entityinsentient.getWidth() + 1.0F);
     }
 
-    public void a() {
+    public synchronized void a() {
         this.a = null;
         this.b = null;
     }
diff --git a/src/main/java/org/yatopiamc/yatopia/server/YatopiaConfig.java b/src/main/java/org/yatopiamc/yatopia/server/YatopiaConfig.java
index 23166aa020077090708a5b01c923e02f10cb8d86..0ca760250ec103844ac09fdacee0d096727d7755 100644
--- a/src/main/java/org/yatopiamc/yatopia/server/YatopiaConfig.java
+++ b/src/main/java/org/yatopiamc/yatopia/server/YatopiaConfig.java
@@ -304,6 +304,12 @@ public class YatopiaConfig {
     public static boolean mcmtlog = true;
     private static void mcmt() {
         mcmtdisabled = getBoolean("mcmt.disabled", mcmtdisabled);
+        
+        if (config.contains("mcmt.disable-world")) {
+            disableWorld = getBoolean("mcmt.disable-world", disableWorld);
+        }
+        
+        // disableWorld = getBoolean("mcmt.disable-world", disableWorld);
         chunkLockModded = getBoolean("mcmt.chunk-lock-modded", chunkLockModded);
         disableChunkCache = getBoolean("mcmt.disable-chunk-cache", disableChunkCache);
         // eventsRunMain = getBoolean("mcmt.events-await-main", eventsRunMain); // TODO - remove config later
diff --git a/src/main/java/net/minecraft/world/entity/ai/village/poi/VillagePlace.java b/src/main/java/net/minecraft/world/entity/ai/village/poi/VillagePlace.java
index 4028526166fe528d772729e9334289df460f3b1e..06174503dd35de334b8e13206e497ec1c89bda8b 100644
--- a/src/main/java/net/minecraft/world/entity/ai/village/poi/VillagePlace.java
+++ b/src/main/java/net/minecraft/world/entity/ai/village/poi/VillagePlace.java
@@ -117,7 +117,7 @@ public class VillagePlace extends RegionFileSection<VillagePlaceSection> {
         }
     }
 
-    public void queueUnload(long coordinate, long minTarget) {
+    public synchronized void queueUnload(long coordinate, long minTarget) { // Yatopia - sync
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async poi unload queue");
         QueuedUnload unload = new QueuedUnload(minTarget + this.determineDelay(coordinate), coordinate);
         QueuedUnload existing = this.queuedUnloadsByCoordinate.put(coordinate, unload);
@@ -127,7 +127,7 @@ public class VillagePlace extends RegionFileSection<VillagePlaceSection> {
         this.queuedUnloads.add(unload);
     }
 
-    public void dequeueUnload(long coordinate) {
+    public synchronized void dequeueUnload(long coordinate) { // Yatopia - sync
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async poi unload dequeue");
         QueuedUnload unload = this.queuedUnloadsByCoordinate.remove(coordinate);
         if (unload != null) {
@@ -135,7 +135,7 @@ public class VillagePlace extends RegionFileSection<VillagePlaceSection> {
         }
     }
 
-    public void pollUnloads(BooleanSupplier canSleepForTick) {
+    public synchronized void pollUnloads(BooleanSupplier canSleepForTick) {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async poi unload");
         long currentTick = net.minecraft.server.MinecraftServer.currentTickLong;
         net.minecraft.server.level.ChunkProviderServer chunkProvider = this.world.getChunkProvider();
