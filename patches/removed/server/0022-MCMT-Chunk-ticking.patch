From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Simon Gardling <titaniumtown@gmail.com>
Date: Wed, 5 May 2021 20:05:06 -0400
Subject: [PATCH] MCMT: Chunk ticking


diff --git a/src/main/java/com/destroystokyo/paper/server/ticklist/PaperTickList.java b/src/main/java/com/destroystokyo/paper/server/ticklist/PaperTickList.java
index b870cca05f0ba354e6976a70511235636093d13c..b552ac0767b499b78c9d999c0f7b4a4570a7875e 100644
--- a/src/main/java/com/destroystokyo/paper/server/ticklist/PaperTickList.java
+++ b/src/main/java/com/destroystokyo/paper/server/ticklist/PaperTickList.java
@@ -53,6 +53,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
 
     // note: remove ops / add ops suck on fastutil, a chained hashtable implementation would work better, but Long...
     // try to alleviate with a very small load factor
+
     private final Long2ObjectOpenHashMap<ArrayList<NextTickListEntry<T>>> entriesByBlock = new Long2ObjectOpenHashMap<>(1024, 0.25f);
     private final Long2ObjectOpenHashMap<ObjectRBTreeSet<NextTickListEntry<T>>> entriesByChunk = new Long2ObjectOpenHashMap<>(1024, 0.25f);
     private final Long2ObjectOpenHashMap<ArrayList<NextTickListEntry<T>>> pendingChunkTickLoad = new Long2ObjectOpenHashMap<>(1024, 0.5f);
@@ -99,7 +100,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
         this.currentTick = this.world.getTime();
     }
 
-    private void queueEntryForTick(final NextTickListEntry<T> entry, final ChunkProviderServer chunkProvider) {
+    private synchronized void queueEntryForTick(final NextTickListEntry<T> entry, final ChunkProviderServer chunkProvider) {
         if (entry.tickState == STATE_SCHEDULED) {
             if (chunkProvider.isTickingReadyMainThread(entry.getPosition())) {
                 this.toTickThisTick.add(entry);
@@ -111,13 +112,13 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
         }
     }
 
-    private void addToNotTickingReady(final NextTickListEntry<T> entry) {
+    private synchronized void addToNotTickingReady(final NextTickListEntry<T> entry) {
         this.pendingChunkTickLoad.computeIfAbsent(MCUtil.getCoordinateKey(entry.getPosition()), (long keyInMap) -> {
             return new ArrayList<>();
         }).add(entry);
     }
 
-    private void addToSchedule(final NextTickListEntry<T> entry) {
+    private synchronized void addToSchedule(final NextTickListEntry<T> entry) {
         long delay = entry.getTargetTick() - (this.currentTick + 1);
         if (delay < SHORT_SCHEDULE_TICK_THRESHOLD) {
             if (delay < 0) {
@@ -131,7 +132,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
         }
     }
 
-    private void removeEntry(final NextTickListEntry<T> entry) {
+    private synchronized void removeEntry(final NextTickListEntry<T> entry) {
         entry.tickState = STATE_CANCELLED_TICK;
         // short/long scheduled will skip the entry
 
@@ -185,7 +186,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
         }
     }
 
-    public void onChunkSetTicking(final int chunkX, final int chunkZ) {
+    public synchronized void onChunkSetTicking(final int chunkX, final int chunkZ) {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async tick list chunk ticking update"); // Tuinity - soft async catcher
         final ArrayList<NextTickListEntry<T>> pending = this.pendingChunkTickLoad.remove(MCUtil.getCoordinateKey(chunkX, chunkZ));
         if (pending == null) {
@@ -199,7 +200,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
         }
     }
 
-    private void prepare() {
+    private synchronized void prepare() {
         final long currentTick = this.currentTick;
 
         final ChunkProviderServer chunkProvider = this.world.getChunkProvider();
@@ -268,7 +269,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
     private boolean warnedAboutDesync;
 
     @Override
-    public void nextTick() {
+    public synchronized void nextTick() {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async tick list tick"); // Tuinity - soft async catcher
         ++this.currentTick;
         if (this.currentTick != this.world.getTime()) {
@@ -281,7 +282,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
     }
 
     @Override
-    public void tick() {
+    public synchronized void tick() {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async tick list tick"); // Tuinity - soft async catcher
         final ChunkProviderServer chunkProvider = this.world.getChunkProvider();
 
@@ -344,7 +345,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
         this.timingFinished.stopTiming();
     }
 
-    private void onTickEnd(final NextTickListEntry<T> entry) {
+    private synchronized void onTickEnd(final NextTickListEntry<T> entry) {
         if (entry.tickState == STATE_CANCELLED_TICK) {
             return;
         }
@@ -383,7 +384,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
     }
 
     @Override
-    public boolean isPendingTickThisTick(final BlockPosition blockposition, final T data) {
+    public synchronized boolean isPendingTickThisTick(final BlockPosition blockposition, final T data) {
         final ArrayList<NextTickListEntry<T>> entries = this.entriesByBlock.get(MCUtil.getBlockKey(blockposition));
 
         if (entries == null) {
@@ -401,7 +402,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
     }
 
     @Override
-    public boolean isScheduledForTick(final BlockPosition blockposition, final T data) {
+    public synchronized boolean isScheduledForTick(final BlockPosition blockposition, final T data) {
         final ArrayList<NextTickListEntry<T>> entries = this.entriesByBlock.get(MCUtil.getBlockKey(blockposition));
 
         if (entries == null) {
@@ -419,15 +420,15 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
     }
 
     @Override
-    public void schedule(BlockPosition blockPosition, T t, int i, TickListPriority tickListPriority) {
+    public synchronized void schedule(BlockPosition blockPosition, T t, int i, TickListPriority tickListPriority) {
         this.schedule(blockPosition, t, i + this.currentTick, tickListPriority);
     }
 
-    public void schedule(final NextTickListEntry<T> entry) {
+    public synchronized void schedule(final NextTickListEntry<T> entry) {
         this.schedule(entry.getPosition(), entry.getData(), entry.getTargetTick(), entry.getPriority());
     }
 
-    public void schedule(final BlockPosition pos, final T data, final long targetTick, final TickListPriority priority) {
+    public synchronized void schedule(final BlockPosition pos, final T data, final long targetTick, final TickListPriority priority) {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async tick list schedule"); // Tuinity - soft async catcher
         final NextTickListEntry<T> entry = new NextTickListEntry<>(pos, data, targetTick, priority);
         if (this.excludeFromScheduling.test(entry.getData())) {
@@ -469,7 +470,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
         this.addToSchedule(entry);
     }
 
-    public void scheduleAll(final Iterator<NextTickListEntry<T>> iterator) {
+    public synchronized void scheduleAll(final Iterator<NextTickListEntry<T>> iterator) {
         while (iterator.hasNext()) {
             this.schedule(iterator.next());
         }
@@ -483,7 +484,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
     }
 
     @Override
-    public List<NextTickListEntry<T>> getEntriesInBoundingBox(final StructureBoundingBox structureboundingbox, final boolean removeReturned, final boolean excludeTicked) {
+    public synchronized List<NextTickListEntry<T>> getEntriesInBoundingBox(final StructureBoundingBox structureboundingbox, final boolean removeReturned, final boolean excludeTicked) {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async tick list get in bounding box"); // Tuinity - soft async catcher
         if (structureboundingbox.getMinX() == structureboundingbox.getMaxX() || structureboundingbox.getMinZ() == structureboundingbox.getMaxZ()) {
             return Collections.emptyList(); // vanilla behaviour, check isBlockInSortof above
@@ -540,7 +541,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
     }
 
     @Override
-    public void copy(StructureBoundingBox structureboundingbox, BlockPosition blockposition) {
+    public synchronized void copy(StructureBoundingBox structureboundingbox, BlockPosition blockposition) {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async tick list copy"); // Tuinity - soft async catcher
         // start copy from TickListServer // TODO check on update
         List<NextTickListEntry<T>> list = this.getEntriesInBoundingBox(structureboundingbox, false, false);
@@ -560,7 +561,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
     }
 
     @Override
-    public List<NextTickListEntry<T>> getEntriesInChunk(ChunkCoordIntPair chunkPos, boolean removeReturned, boolean excludeTicked) {
+    public synchronized List<NextTickListEntry<T>> getEntriesInChunk(ChunkCoordIntPair chunkPos, boolean removeReturned, boolean excludeTicked) {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async tick list get"); // Tuinity - soft async catcher
         // Vanilla DOES get the entries 2 blocks out of the chunk too, but that doesn't matter since we ignore chunks
         // not at ticking status, and ticking status requires neighbours loaded
@@ -592,7 +593,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
     }
 
     @Override
-    public NBTTagList serialize(ChunkCoordIntPair chunkcoordintpair) {
+    public synchronized NBTTagList serialize(ChunkCoordIntPair chunkcoordintpair) {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async tick list serialize"); // Tuinity - soft async catcher
         // start copy from TickListServer  // TODO check on update
         List<NextTickListEntry<T>> list = this.getEntriesInChunk(chunkcoordintpair, false, true);
@@ -602,7 +603,7 @@ public final class PaperTickList<T> extends TickListServer<T> { // extend to avo
     }
 
     @Override
-    public int getTotalScheduledEntries() {
+    public synchronized int getTotalScheduledEntries() {
         com.tuinity.tuinity.util.TickThread.softEnsureTickThread("async tick list get size"); // Tuinity - soft async catcher
         // good thing this is only used in debug reports // TODO check on update
         int ret = 0;
diff --git a/src/main/java/net/himeki/mcmtfabric/ParallelProcessor.java b/src/main/java/net/himeki/mcmtfabric/ParallelProcessor.java
index 9fcaf9f051b2200c484427e1c00c833c977d15ec..aa91cb2cef02ce083e1a337e74c03499b29ff63f 100644
--- a/src/main/java/net/himeki/mcmtfabric/ParallelProcessor.java
+++ b/src/main/java/net/himeki/mcmtfabric/ParallelProcessor.java
@@ -283,6 +283,23 @@ public class ParallelProcessor {
         }
     }
 
+    public static void tickChunk(WorldServer world, Chunk chunk, int i) {
+        if (YatopiaConfig.disableChunks) {
+            world.tickChunk(chunk, i);
+        } else {
+            phaser.register();
+            ex.execute(() -> {
+                try {
+                    currentEnvironments.incrementAndGet();
+                    world.tickChunk(chunk, i);
+                } finally {
+                    currentEnvironments.decrementAndGet();
+                    phaser.arriveAndDeregister();
+                }
+            });
+        }
+    }
+
     public static <T> void fixSTL(TickListServer<T> stl, TreeSet<NextTickListEntry<T>> scheduledTickActionsInOrder, Set<NextTickListEntry<T>> scheduledTickActions) {
         if (YatopiaConfig.mcmtlog) LOGGER.debug("FixSTL Called");
         scheduledTickActionsInOrder.addAll(scheduledTickActions);
diff --git a/src/main/java/net/minecraft/server/level/ChunkProviderServer.java b/src/main/java/net/minecraft/server/level/ChunkProviderServer.java
index ca80d294558201edd0aa9e049f88a82c42673a58..a652a1064ef00d040285b8576a71f99f23869b62 100644
--- a/src/main/java/net/minecraft/server/level/ChunkProviderServer.java
+++ b/src/main/java/net/minecraft/server/level/ChunkProviderServer.java
@@ -1211,7 +1211,8 @@ public class ChunkProviderServer extends IChunkProvider {
                             }
 
                             this.world.timings.chunkTicks.startTiming(); // Spigot // Paper
-                            this.world.a(chunk, k);
+                            // this.world.a(chunk, k);
+                            ParallelProcessor.tickChunk(world, chunk, k);
                             this.world.timings.chunkTicks.stopTiming(); // Spigot // Paper
                             if ((++ticked & 1) == 0) MinecraftServer.getServer().executeMidTickTasks(); // Tuinity - exec chunk tasks during world tick
                         }
diff --git a/src/main/java/net/minecraft/server/level/LightEngineThreaded.java b/src/main/java/net/minecraft/server/level/LightEngineThreaded.java
index 229bc64e65696319a83a0f7a7ba6f75752de44cd..a934887b7b9fc31a04bfce288b8bf576b9ebc40c 100644
--- a/src/main/java/net/minecraft/server/level/LightEngineThreaded.java
+++ b/src/main/java/net/minecraft/server/level/LightEngineThreaded.java
@@ -569,7 +569,7 @@ public class LightEngineThreaded extends LightEngine implements AutoCloseable {
         // Paper end
     }
 
-    public void queueUpdate() {
+    public synchronized void queueUpdate() {
         if ((!this.queue.isEmpty() || (this.theLightEngine == null && super.a())) && this.g.compareAndSet(false, true)) { // Paper // Tuinity - replace light impl
             this.b.a((() -> { // Paper - decompile error
                 this.b();
@@ -583,7 +583,7 @@ public class LightEngineThreaded extends LightEngine implements AutoCloseable {
     // Paper start - replace impl
     private final java.util.List<Runnable> pre = new java.util.ArrayList<>();
     private final java.util.List<Runnable> post = new java.util.ArrayList<>();
-    private void b() {
+    private synchronized void b() {
         //final long start = System.nanoTime(); // TODO remove debug
         if (queue.poll(pre, post)) {
             pre.forEach(Runnable::run);
diff --git a/src/main/java/net/minecraft/world/level/TickListServer.java b/src/main/java/net/minecraft/world/level/TickListServer.java
index 4fd89bbe6ce578fd3a166bcfbbe41908a7bb4753..22cd65b79c6326eea7f1ceda222017e6a9a8d954 100644
--- a/src/main/java/net/minecraft/world/level/TickListServer.java
+++ b/src/main/java/net/minecraft/world/level/TickListServer.java
@@ -26,13 +26,16 @@ import net.minecraft.server.level.ChunkProviderServer;
 import net.minecraft.server.level.WorldServer;
 import net.minecraft.world.level.block.state.IBlockData;
 import net.minecraft.world.level.levelgen.structure.StructureBoundingBox;
+import net.himeki.mcmtfabric.ParallelProcessor;
+import org.jmt.mcmt.paralelised.ConcurrentCollections;
 
 public class TickListServer<T> implements TickList<T> {
 
     protected final Predicate<T> a;
     private final Function<T, MinecraftKey> b;
-    private final Set<NextTickListEntry<T>> nextTickListHash = Sets.newHashSet();
-    private final TreeSet<NextTickListEntry<T>> nextTickList = Sets.newTreeSet(NextTickListEntry.a());
+    // private final Set<NextTickListEntry<T>> nextTickListHash = Sets.newHashSet(); // scheduledTickActions
+    private final Set<NextTickListEntry<T>> nextTickListHash = ConcurrentCollections.newHashSet(); // scheduledTickActions
+    private final TreeSet<NextTickListEntry<T>> nextTickList = Sets.newTreeSet(NextTickListEntry.a()); // scheduledTickActionsInOrder
     private final WorldServer e;
     private final Queue<NextTickListEntry<T>> f = Queues.newArrayDeque();
     private final List<NextTickListEntry<T>> g = Lists.newArrayList();
@@ -58,9 +61,10 @@ public class TickListServer<T> implements TickList<T> {
         // Paper start - allow overriding
         this.tick();
     }
-    public void tick() {
+    public synchronized void tick() {
         // Paper end
-        int i = this.nextTickList.size();
+        // int i = this.nextTickList.size();
+        int i = this.nextTickListHash.size();
 
         if (false) { // CraftBukkit
             throw new IllegalStateException("TickNextTick list out of synch");
@@ -75,6 +79,9 @@ public class TickListServer<T> implements TickList<T> {
                 // CraftBukkit end
             }
 
+            if (this.nextTickListHash.size() != this.nextTickList.size()) {
+                ParallelProcessor.fixSTL((TickListServer<T>) (Object) this, nextTickList, nextTickListHash);
+            }
             ChunkProviderServer chunkproviderserver = this.e.getChunkProvider();
             Iterator<NextTickListEntry<T>> iterator = this.nextTickList.iterator();
 
@@ -153,7 +160,7 @@ public class TickListServer<T> implements TickList<T> {
         // Paper start - allow overriding
         return this.getEntriesInBoundingBox(structureboundingbox, flag, flag1);
     }
-    public List<NextTickListEntry<T>> getEntriesInBoundingBox(StructureBoundingBox structureboundingbox, boolean flag, boolean flag1) {
+    public synchronized List<NextTickListEntry<T>> getEntriesInBoundingBox(StructureBoundingBox structureboundingbox, boolean flag, boolean flag1) {
         // Paper end
         List<NextTickListEntry<T>> list = this.a((List) null, this.nextTickList, structureboundingbox, flag);
 
@@ -170,7 +177,7 @@ public class TickListServer<T> implements TickList<T> {
     }
 
     @Nullable
-    private List<NextTickListEntry<T>> a(@Nullable List<NextTickListEntry<T>> list, Collection<NextTickListEntry<T>> collection, StructureBoundingBox structureboundingbox, boolean flag) {
+    private synchronized List<NextTickListEntry<T>> a(@Nullable List<NextTickListEntry<T>> list, Collection<NextTickListEntry<T>> collection, StructureBoundingBox structureboundingbox, boolean flag) {
         Iterator iterator = collection.iterator();
 
         while (iterator.hasNext()) {
@@ -262,7 +269,7 @@ public class TickListServer<T> implements TickList<T> {
         // Paper start - allow overriding
         this.schedule(blockposition, t0, i, ticklistpriority);
     }
-    public void schedule(BlockPosition blockposition, T t0, int i, TickListPriority ticklistpriority) {
+    public synchronized void schedule(BlockPosition blockposition, T t0, int i, TickListPriority ticklistpriority) {
         // Paper end
         if (!this.a.test(t0)) {
             this.a(new NextTickListEntry<>(blockposition, t0, (long) i + this.e.getTime(), ticklistpriority));
@@ -270,7 +277,8 @@ public class TickListServer<T> implements TickList<T> {
 
     }
 
-    private void a(NextTickListEntry<T> nextticklistentry) {
+    private synchronized void addEntry(NextTickListEntry<T> nextticklistentry) { this.a(nextticklistentry); } // Yatopia - OBFHELPER
+    private synchronized void a(NextTickListEntry<T> nextticklistentry) {
         if (!this.nextTickListHash.contains(nextticklistentry)) {
             this.nextTickListHash.add(nextticklistentry);
             this.nextTickList.add(nextticklistentry);
diff --git a/src/main/java/net/minecraft/world/level/World.java b/src/main/java/net/minecraft/world/level/World.java
index 16c5a07f89f5b711c75136020728e5578aef049d..db9d7019c326db2831cbbd89a04d2517fea03114 100644
--- a/src/main/java/net/minecraft/world/level/World.java
+++ b/src/main/java/net/minecraft/world/level/World.java
@@ -648,15 +648,15 @@ public abstract class World implements GeneratorAccess, AutoCloseable, NonBlocki
     }
 
     @Override
-    public synchronized boolean a(BlockPosition blockposition, IBlockData iblockdata, int i, int j) { // Yatopia - sync
+    public boolean a(BlockPosition blockposition, IBlockData iblockdata, int i, int j) { // Yatopia - sync
         org.spigotmc.AsyncCatcher.catchOp("set type call"); // Tuinity
-        /*
+        
         if (net.himeki.mcmtfabric.ParallelProcessor.catchOp()) {
             return (Boolean)net.himeki.mcmtfabric.ParallelProcessor.await(net.himeki.mcmtfabric.ParallelProcessor.main(() -> {
                 return this.a(blockposition, iblockdata, i, j);
             }));
         }
-        */
+        
         // CraftBukkit start - tree generation
         if (this.captureTreeGeneration) {
             // Paper start
diff --git a/src/main/java/net/minecraft/world/level/lighting/LightEngineGraph.java b/src/main/java/net/minecraft/world/level/lighting/LightEngineGraph.java
index f52e52070475d8e869a311a99c93b875667c364b..da2a94764492a9d19f5878d92a6e8e5f120c4170 100644
--- a/src/main/java/net/minecraft/world/level/lighting/LightEngineGraph.java
+++ b/src/main/java/net/minecraft/world/level/lighting/LightEngineGraph.java
@@ -6,12 +6,14 @@ import it.unimi.dsi.fastutil.longs.LongArrayList;
 import it.unimi.dsi.fastutil.longs.LongLinkedOpenHashSet;
 import java.util.function.LongPredicate;
 import net.minecraft.util.MathHelper;
+import org.jmt.mcmt.paralelised.fastutil.Long2ByteConcurrentHashMap;
+import org.jmt.mcmt.paralelised.fastutil.sync.SyncLongLinkedOpenHashSet;
 
 public abstract class LightEngineGraph {
 
     private final int a;
-    private final LongLinkedOpenHashSet[] b;
-    private final Long2ByteMap c;
+    private final LongLinkedOpenHashSet[] b; // pendingIdUpdatesByLevel
+    private Long2ByteMap c; // pendingUpdates
     private int d;
     private volatile boolean e;
 
@@ -20,10 +22,10 @@ public abstract class LightEngineGraph {
             throw new IllegalArgumentException("Level count must be < 254.");
         } else {
             this.a = i;
-            this.b = new LongLinkedOpenHashSet[i];
+            this.b = new SyncLongLinkedOpenHashSet[i];
 
             for (int l = 0; l < i; ++l) {
-                this.b[l] = new LongLinkedOpenHashSet(j, 0.5F) {
+                this.b[l] = new SyncLongLinkedOpenHashSet(j, 0.5F) {
                     protected void rehash(int i1) {
                         if (i1 > j) {
                             super.rehash(i1);
@@ -74,6 +76,7 @@ public abstract class LightEngineGraph {
 
     }
 
+    protected void removeFromQueue(long i) { this.e(i); } // Yatopia - OBFHELPER
     protected void e(long i) {
         int j = this.c.get(i) & 255;
 
@@ -89,13 +92,17 @@ public abstract class LightEngineGraph {
     public void a(LongPredicate longpredicate) {
         LongArrayList longarraylist = new LongArrayList();
 
-        this.c.keySet().forEach((i) -> {
+        // Yatopia start - decomp fixes
+        for (long i : this.c.keySet()) {
             if (longpredicate.test(i)) {
                 longarraylist.add(i);
             }
+        }
 
-        });
-        longarraylist.forEach(this::e);
+        for (long i : longarraylist) {
+            this.removeFromQueue(i);
+        }
+        // Yatopia end
     }
 
     private void a(long i, int j, int k, boolean flag) {
@@ -123,7 +130,7 @@ public abstract class LightEngineGraph {
         this.a(i, i, this.a - 1, false);
     }
 
-    protected void a(long i, long j, int k, boolean flag) {
+    protected synchronized void a(long i, long j, int k, boolean flag) {
         this.a(i, j, k, this.c(j), this.c.get(j) & 255, flag);
         this.e = this.d < this.a;
     }
@@ -195,7 +202,7 @@ public abstract class LightEngineGraph {
         return this.e;
     }
 
-    protected final int b(int i) {
+    protected synchronized final int b(int i) { // tick function
         if (this.d >= this.a) {
             return i;
         } else {
diff --git a/src/main/java/net/minecraft/world/level/pathfinder/PathfinderNormal.java b/src/main/java/net/minecraft/world/level/pathfinder/PathfinderNormal.java
index cc828b755be92627f2cb65e6e21fafe813fb1a51..e70bb60eee1377fc8caf16a93c69bc52c4c106ab 100644
--- a/src/main/java/net/minecraft/world/level/pathfinder/PathfinderNormal.java
+++ b/src/main/java/net/minecraft/world/level/pathfinder/PathfinderNormal.java
@@ -34,7 +34,10 @@ import net.minecraft.world.phys.shapes.VoxelShape;
 public class PathfinderNormal extends PathfinderAbstract {
 
     protected float j;
-    private final Long2ObjectMap<PathType> k = new Long2ObjectOpenHashMap();
+    // private final Long2ObjectMap<PathType> k = new Long2ObjectOpenHashMap();
+    private final Long2ObjectMap<PathType> k = new org.yatopiamc.yatopia.server.util.ConcLong2ObjectOpenHashMap();
+    
+
     private final Object2BooleanMap<AxisAlignedBB> l = new Object2BooleanOpenHashMap();
 
     public PathfinderNormal() {}
@@ -46,7 +49,7 @@ public class PathfinderNormal extends PathfinderAbstract {
     }
 
     @Override
-    public void a() {
+    public synchronized void a() {
         this.b.a(PathType.WATER, this.j);
         this.k.clear();
         this.l.clear();
@@ -526,6 +529,7 @@ public class PathfinderNormal extends PathfinderAbstract {
     }
 
     protected static PathType b(IBlockAccess iblockaccess, BlockPosition blockposition) {
+        if (blockposition == null) return PathType.BLOCKED; // Yatopia
         IBlockData iblockdata = iblockaccess.getTypeIfLoaded(blockposition); // Paper
         if (iblockdata == null) return PathType.BLOCKED; // Paper
         // Tuinity start - reduce pathfinder branches
diff --git a/src/main/java/org/yatopiamc/yatopia/server/YatopiaConfig.java b/src/main/java/org/yatopiamc/yatopia/server/YatopiaConfig.java
index 1396e146d0b13aaf1742c16acb742cf37dc6d2a2..4155cd2139efd7f6da37f8ade8da460055d3f648 100644
--- a/src/main/java/org/yatopiamc/yatopia/server/YatopiaConfig.java
+++ b/src/main/java/org/yatopiamc/yatopia/server/YatopiaConfig.java
@@ -306,6 +306,7 @@ public class YatopiaConfig {
     public static boolean mcmtlog = true;
     public static boolean asyncSpawnerCreature = false;
     public static boolean flushTasks = false;
+    public static boolean disableChunks = false;
     private static void mcmt() {
         mcmtdisabled = getBoolean("mcmt.disabled", mcmtdisabled);
         chunkLockModded = getBoolean("mcmt.chunk-lock-modded", chunkLockModded);
@@ -316,6 +317,7 @@ public class YatopiaConfig {
         asyncSpawnerCreature = getBoolean("mcmt.async-spawnercreature", asyncSpawnerCreature);
         changeMCMTStatus(mcmtdisabled);
         flushTasks = getBoolean("mcmt.flush-tasks", flushTasks);
+        disableChunks = getBoolean("mcmt.disable-chunks", disableChunks);
     }
 
     public static void changeMCMTStatus(boolean disabled) { // function to enable or disable mcmt while affecting other configs
@@ -324,6 +326,7 @@ public class YatopiaConfig {
             disableEntity = true;
             mcmtdisabled = true;
             eventsRunMain = false;
+            disableChunks = true;
         } else {
             mcmtdisabled = false;
             eventsRunMain = true;
