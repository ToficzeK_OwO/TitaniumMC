pipeline {
    agent any
    options { timestamps() }
    environment {
        discord_webhook1 = credentials('yatopia_dev_titaniummc_webhook')
    }
    stages {
        stage('Cleanup') {
            tools {
                jdk "graalvm 11"
            }
            steps {
                updateGitlabCommitStatus name: 'clean', state: 'pending'
                sh 'rm -rf ./target'

                sh 'rm -rf ./Yatopia/Paper/Paper-API ./Yatopia/Paper/Paper-Server'
                sh 'mv ./Yatopia/Paper/work/Minecraft ./ || true' 
                sh 'rm -fr ./Yatopia/Paper/work/*'
                sh 'mv ./Minecraft ./Yatopia/Paper/work/ || true'

                sh 'rm -rf ./Yatopia/Yatopia-API ./Yatopia/Yatopia-Server'
                sh 'rm -rf ./TitaniumMC-API ./TitaniumMC-Server ./TitaniumMC-Server_yarn mappings/work/*'
                sh 'git clean -f -d -x'
                sh 'cd ./Yatopia && git clean -f -d -x'
                sh 'chmod +x ./gradlew'
                updateGitlabCommitStatus name: 'clean', state: 'success'
            }
        }

        stage('initGitSubmodules') {
            tools {
                jdk "graalvm 11"
            }
            steps {
                updateGitlabCommitStatus name: 'initGitSubmodules', state: 'pending'
                sh './gradlew initGitSubmodules'
                updateGitlabCommitStatus name: 'initGitSubmodules', state: 'success'
            }
        }

        stage('setupUpstream & applyPatches') {
            tools {
                jdk "graalvm 11"
            }
            steps {
                updateGitlabCommitStatus name: 'applyPatches', state: 'pending'
                sh '''
                git config --global user.email "you@example.com"
                git config --global user.name "Your Name"
                ./gradlew setupUpstream
                ./gradlew applyPatches
                '''
                updateGitlabCommitStatus name: 'applyPatches', state: 'success'
            }
        }

        stage('Build') {
            tools {
                jdk "graalvm 11"
            }
            steps {
                updateGitlabCommitStatus name: 'build', state: 'pending'
                sh '''
                git config --global user.email "you@example.com"
                git config --global user.name "Your Name"
                ./gradlew clean 
                ./gradlew build -x test paperclip
                mkdir -p "./target"
                cp "launcher-titaniummc.jar" "./target/titaniummc-paperclip-b$BUILD_NUMBER.jar"
                '''
                updateGitlabCommitStatus name: 'build', state: 'success'
            }
            post {
                success {
                    archiveArtifacts artifacts: 'target/*.jar', fingerprint: true
                }
                failure {
                    cleanWs()
                }
                always {
                    discordSend description: "TitaniumMC Jenkins Build", footer: "TitaniumMC", link: env.BUILD_URL, result: currentBuild.currentResult, title: JOB_NAME, webhookURL: discord_webhook1
                }
            }
        }
    }
}
